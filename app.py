from flask import Flask,request, render_template, flash, redirect,url_for
from models import *

#[('email', ''), ('password', ''), ('fname', ''), ('lname', ''), ('content', ''), ('name', ''), ('title', ''), ('tag', '')]

app= Flask(__name__)

@app.route('/')
def index():
	posts = Post.objects.all()
	return render_template('index.html',  p=posts)


@app.route('/posts', methods=['GET', 'POST'])
def posts():
	error = None
	if request.method == 'POST':
		try:
			user = User(email=request.form['email'],password=request.form['password'],last_name=request.form['lname'], first_name=request.form['fname'])
			user.save()
			comment = Comment(content=request.form['content'],name=request.form['name'])
			comment.save()
			post = Post(title=request.form['title'],author=user,tags=request.form['tag'],comments=comment)
			post.save()
			return redirect(url_for('index'))
		except:
			flash('User does not exist')
	return render_template('posts.html', error=error)
	

if __name__ == '__main__': 
    app.run()
